angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {
  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  // $ionicModal.fromTemplateUrl('templates/login.html', {
  //   scope: $scope
  // }).then(function(modal) {
  //   $scope.modal = modal;
  // });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})
.controller('CategoriesController',['$http', function($http) {
  var quiz = this;
  quiz.quizzes = [];
  $http.get('data/questions.json').success(function(data){
    quiz.quizzes = data;
  });
}])

.controller('SignUpController', function ($scope , Camera) {
  $scope.getPhoto = function () {
    Camera.getPicture({
      quality: 50,
      targetWidth: 320,
      targetHeight: 320,
      saveToPhotoAlbum: false
    }).then(function (imageURI) {
      $scope.lastPhoto = imageURI;
    }, function (err) {
      console.err(err);
    });
  };
})

.controller('QuizViewController',['$scope','$http','$stateParams',function($scope,$http,$stateParams){
  $http.get('data/questions.json').success(function(data){
    $scope.quiz = data[$stateParams.quizId];
    $scope.randomVal = function() {
        return 0.5 - Math.random();
    }    
  })

}])


.controller( 'DashboardController', function( $scope ) {
    $scope.loadedStatus = '';
  $scope.$on('$ionicView.loaded', function(){
    $scope.loadedStatus = 'loaded';
  });

// Chart.js Data
    $scope.data = [
      {
        value: 100,
        color:'#F7464A',
        highlight: '#FF5A5E',
        label: 'Theme 1'
      },
      {
        value: 20,
        color: '#46BFBD',
        highlight: '#5AD3D1',
        label: 'Theme 2'
      },
      {
        value: 55,
        color: '#FDB45C',
        highlight: '#FFC870',
        label: 'Theme 3'
      }
    ];

    // Chart.js Options
    $scope.options =  {

      // Sets the chart to be responsive
      responsive: true,

      //Boolean - Whether we should show a stroke on each segment
      segmentShowStroke : true,

      //String - The colour of each segment stroke
      segmentStrokeColor : '#fff',

      //Number - The width of each segment stroke
      segmentStrokeWidth : 2,

      //Number - The percentage of the chart that we cut out of the middle
      percentageInnerCutout : 50, // This is 0 for Pie charts

      //Number - Amount of animation steps
      animationSteps : 100,

      //String - Animation easing effect
      animationEasing : 'easeOutBounce',

      //Boolean - Whether we animate the rotation of the Doughnut
      animateRotate : true,

      //Boolean - Whether we animate scaling the Doughnut from the centre
      animateScale : false,

      //String - A legend template
      legendTemplate : '<ul class="tc-chart-js-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>'

    };

});

