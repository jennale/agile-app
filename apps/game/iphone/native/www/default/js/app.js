
/* JavaScript content from js/app.js in folder common */
// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers','starter.services','tc.chartjs'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  .state('landing',{
    url:'/',
    templateUrl: 'templates/landing.html'
  })

  .state('login',{
    url:'/login',
    templateUrl: 'templates/sign-in.html'
  })

  .state('signup',{
    url:'/signup',
    templateUrl:'templates/sign-up.html',
    controller:'SignUpController'

  })
  .state('quiz', {
    url: '/quiz',
    abstract: true, //A "parent" template 
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })
  .state('quiz.dashboard',{
    url:'/dash',
    views: {
      'menuContent':{
        templateUrl: 'templates/dashboard.html',
        controller: 'DashboardController'
      }
    }
  })
  .state('quiz.select', {
    url: '/select',
    views: {
      'menuContent': {
        templateUrl: 'templates/quiz-select.html',
        controller: 'CategoriesController',
        controllerAs: 'categories'
      }
    }
  })
  .state('quiz.view',{
    url:'/view/:quizId',
    views:{
      'menuContent': {
        templateUrl:'templates/quiz-view.html',
        controller:'QuizViewController'
      }
    }
  })
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/');
})
.directive('appReady', function() {
  return {
    restrict: 'A',
    link: function(scope, element, attrs){
      console.log("Done rendering body");
      WL.App.hideSplashScreen();
    }
  };
});
